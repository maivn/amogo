package amogo

type Tags []Tag

type Tag struct {
	Id   int64  `json:"id"`
	Name string `json:"name"`
}

func (tags *Tags) GetById(id int64) *Tag {
	for _, tag := range *tags {
		if tag.Id == id {
			return &tag
		}
	}
	return nil
}
