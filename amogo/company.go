package amogo

import (
	"net/url"
	"github.com/json-iterator/go"
)

type ResponseCompanies struct {
	Embedded EmbeddedCompanies `json:"_embedded"`
}

type EmbeddedCompanies struct {
	Items Companies `json:"items"`
}

type Companies []Company

type Company struct {
	Id                int64        `json:"id"`
	Name              string       `json:"name"`
	ResponsibleUserId int64        `json:"responsible_user_id"`
	CreatedBy         int64        `json:"created_by"`
	CreatedAt         int64        `json:"created_at"`
	UpdatedAt         int64        `json:"updated_at"`
	AccountId         int64        `json:"account_id"`
	UpdatedBy         int64        `json:"updated_by"`
	GroupId           int64        `json:"group_id"`
	Contacts          ContactsLink `json:"contacts"`
	Leads             LeadsLink    `json:"leads"`
	ClosestTaskAt     int64        `json:"closest_task_at"`
	Tags              Tags         `json:"tags"`
	CustomFields      CustomFields `json:"custom_fields"`
	Customers         interface{}
}

func (companies *Companies) GetById(id int64) *Company {
	for _, company := range *companies {
		if company.Id == id {
			return &company
		}
	}
	return nil
}

type CompanyLink struct {
	Id    int64  `json:"id"`
	Name  string `json:"name"`
	Links Links  `json:"_links"`
}

func (amo *Amo) CompanyList(values url.Values) (companies Companies, err error) {
	u := url.URL{
		Scheme:   "https",
		Host:     amo.Domain,
		Path:     "/api/v2/companies",
		RawQuery: values.Encode(),
	}

	data, err := amo.get(u)
	if err != nil {
		return
	}

	/*if len(data) == 0 {
		err = ErrEntityNotFound
		return
	}*/

	var response ResponseCompanies
	err = jsoniter.Unmarshal(data, &response)
	if err != nil {
		return
	}

	companies = response.Embedded.Items
	return
}

func (amo *Amo) AddCompanies(companies ...Company) (addCompanies Companies, err error) {
	u := url.URL{
		Scheme: "https",
		Host:   amo.Domain,
		Path:   "/api/v2/companies",
	}

	data, err := amo.post(u, struct {
		Add Companies `json:"add"`
	}{companies})
	if err != nil {
		return
	}

	var response ResponseCompanies
	err = jsoniter.Unmarshal(data, &response)
	addCompanies = response.Embedded.Items
	return
}

func (amo *Amo) UpdateCompanies(companies ...Company) (updateCompanies Companies, err error) {
	u := url.URL{
		Scheme: "https",
		Host:   amo.Domain,
		Path:   "/api/v2/companies",
	}

	data, err := amo.post(u, struct {
		Update Companies `json:"update"`
	}{companies})
	if err != nil {
		return
	}

	var response ResponseCompanies
	err = jsoniter.Unmarshal(data, &response)
	updateCompanies = response.Embedded.Items
	return
}
