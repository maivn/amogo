package amogo

import (
	"net/url"
	"github.com/json-iterator/go"
)

type ResponseWebHooks struct {
	Embedded EmbeddedWebHooks `json:"_embedded"`
}

type EmbeddedWebHooks struct {
	Items WebHooks `json:"items"`
}

type WebHooks []WebHook

type WebHook struct {
	Id       int64    `json:"id" form:"id"`
	Url      string   `json:"url" form:"url"`
	Disabled bool     `json:"disabled" form:"disabled"`
	Events   []string `json:"events" form:"events"`
}

func (amo *Amo) WebHookList() (webHooks WebHooks, err error) {
	u := url.URL{
		Scheme: "https",
		Host:   amo.Domain,
		Path:   "/api/v2/webhooks",
	}

	data, err := amo.get(u)
	if err != nil {
		return
	}

	/*if len(data) == 0 {
		err = ErrEntityNotFound
		return
	}*/

	var response ResponseWebHooks
	err = jsoniter.Unmarshal(data, &response)
	if err != nil {
		return
	}

	webHooks = response.Embedded.Items
	return
}

func (amo *Amo) AddWebHooks(webHooks ...WebHook) (addWebHooks WebHooks, err error) {
	u := url.URL{
		Scheme: "https",
		Host:   amo.Domain,
		Path:   "/api/v2/webhooks/subscribe",
	}

	data, err := amo.post(u, struct {
		Subscribe WebHooks `json:"subscribe"`
	}{webHooks})
	if err != nil {
		return
	}

	var response ResponseWebHooks
	err = jsoniter.Unmarshal(data, &response)
	addWebHooks = response.Embedded.Items
	return
}

func (amo *Amo) UpdateWebHooks(webHooks ...WebHook) (updateWebHooks WebHooks, err error) {
	u := url.URL{
		Scheme: "https",
		Host:   amo.Domain,
		Path:   "/api/v2/webhooks",
	}

	data, err := amo.post(u, struct {
		Update WebHooks `json:"update"`
	}{webHooks})

	if err != nil {
		return
	}

	var response ResponseWebHooks
	err = jsoniter.Unmarshal(data, &response)
	updateWebHooks = response.Embedded.Items
	return
}
