package amogo

import "errors"

type Error struct {
	Response ResponseError `json:"response"`
}
type ResponseError struct {
	ErrorCode    int64  `json:"error_code"`
	ErrorMessage string `json:"error"`
	Ip           string `json:"ip"`
	Domain       string `json:"domain"`
	ServerTime   int64  `json:"server_time"`
}

func (responseError *ResponseError) Error() string {
	return responseError.ErrorMessage
}

var (
	ErrEntityNotFound = errors.New("record not found")
)

func IsErrEntityNotFound(err error) bool {
	if err == ErrEntityNotFound {
		return true
	}
	return false
}
