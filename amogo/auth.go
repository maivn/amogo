package amogo

import (
	"net/url"
	"encoding/json"
	"fmt"
)

type Auth struct {
	Response struct {
		Error      string `json:"error"`
		Ip         string `json:"ip"`
		Domain     string `json:"domain"`
		Auth       bool   `json:"auth"`
		ServerTime int64  `json:"server_time"`
		ErrorCode  string `json:"error_code"`
	} `json:"response"`
}

func (amo *Amo) auth() (err error) {
	u := url.URL{
		Scheme: "https",
		Host:   amo.Domain,
		Path:   "/private/api/auth.php",
		RawQuery: url.Values{
			"type": {"json"},
		}.Encode(),
	}

	values := url.Values{
		"USER_LOGIN": {amo.Login},
		"USER_HASH":  {amo.Hash},
	}

	data, err := amo.postForm(u, values)
	if err != nil {
		return
	}

	var auth Auth
	err = json.Unmarshal(data, &auth)
	if err != nil {
		return
	}

	if !auth.Response.Auth {
		return fmt.Errorf("Ошибка авторизации: " + auth.Response.Error)
	}
	return
}
