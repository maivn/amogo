package amogo

import (
	"github.com/json-iterator/go"
	"unsafe"
	"encoding/json"
)

func init() {
	jsoniter.RegisterTypeDecoderFunc("amogo.MapCustomFields", func(ptr unsafe.Pointer, iter *jsoniter.Iterator) {
		switch a := iter.WhatIsNext(); a {
		case jsoniter.ObjectValue:
			var mapCustomFields MapCustomFields
			data, _ := json.Marshal(iter.Read())
			json.Unmarshal(data, &mapCustomFields)
			*(*MapCustomFields)(ptr) = mapCustomFields
		default:
			iter.Skip()
		}
	})

	jsoniter.RegisterTypeDecoderFunc("amogo.Tags", func(ptr unsafe.Pointer, iter *jsoniter.Iterator) {
		switch iter.WhatIsNext() {
		case jsoniter.ArrayValue:
			var tags Tags
			data, _ := json.Marshal(iter.Read())
			json.Unmarshal(data, &tags)
			*(*Tags)(ptr) = tags
		default:
			iter.Skip()
		}
	})

	jsoniter.RegisterTypeDecoderFunc("amogo.Status", func(ptr unsafe.Pointer, iter *jsoniter.Iterator) {
		switch iter.WhatIsNext() {
		case jsoniter.ObjectValue:
			var status Status
			data, _ := json.Marshal(iter.Read())
			json.Unmarshal(data, &status)
			*(*Status)(ptr) = status
		default:
			iter.Skip()
		}
	})

	jsoniter.RegisterTypeDecoderFunc("amogo.CustomFields", func(ptr unsafe.Pointer, iter *jsoniter.Iterator) {
		switch iter.WhatIsNext() {
		case jsoniter.ArrayValue:
			var customFields CustomFields
			data, _ := json.Marshal(iter.Read())
			json.Unmarshal(data, &customFields)
			*(*CustomFields)(ptr) = customFields
		default:
			iter.Skip()
		}
	})

	jsoniter.RegisterTypeDecoderFunc("amogo.MapGroups", func(ptr unsafe.Pointer, iter *jsoniter.Iterator) {
		switch a := iter.WhatIsNext(); a {
		case jsoniter.ObjectValue:
			var mapGroups MapGroups
			data, _ := json.Marshal(iter.Read())
			json.Unmarshal(data, &mapGroups)
			*(*MapGroups)(ptr) = mapGroups
		default:
			iter.Skip()
		}
	})

	/*
	//TEXT
	jsoniter.RegisterFieldDecoderFunc("amogo.TextValue", "Value", func(ptr unsafe.Pointer, iter *jsoniter.Iterator) {
		switch iter.WhatIsNext() {
		case jsoniter.NilValue:
			*(**string)(ptr) = nil
		case jsoniter.StringValue:
			str := iter.ReadString()
			*(**string)(ptr) = &str
		default:
			iter.Skip()
		}
	})

	jsoniter.RegisterFieldEncoderFunc("amogo.TextValue", "Value", func(ptr unsafe.Pointer, stream *jsoniter.Stream) {
		switch p := *(**string)(unsafe.Pointer(ptr)); p {
		case nil:
			stream.WriteString("")
		default:
			stream.WriteString(*p)
		}
	}, func(pointer unsafe.Pointer) bool { return false })

	//NUMERIC
	jsoniter.RegisterFieldDecoderFunc("amogo.NumericValue", "Value", func(ptr unsafe.Pointer, iter *jsoniter.Iterator) {
		switch iter.WhatIsNext() {
		case jsoniter.NilValue:
			*(**string)(ptr) = nil
		case jsoniter.NumberValue:
			n := iter.ReadInt64()
			*(**int64)(ptr) = &n
		case jsoniter.StringValue:
			n, _ := strconv.ParseInt(iter.ReadString(), 10, 0)
			*(**int64)(ptr) = &n
		default:
			iter.Skip()
		}
	})

	jsoniter.RegisterFieldEncoderFunc("amogo.NumericValue", "Value", func(ptr unsafe.Pointer, stream *jsoniter.Stream) {
		switch p := *(**int64)(unsafe.Pointer(ptr)); p {
		case nil:
			stream.WriteString("")
		default:
			stream.WriteInt64(*p)
		}
	}, func(pointer unsafe.Pointer) bool { return false })

	//CHECKBOX
	jsoniter.RegisterFieldDecoderFunc("amogo.CheckboxValue", "Value", func(ptr unsafe.Pointer, iter *jsoniter.Iterator) {
		switch iter.WhatIsNext() {
		case jsoniter.NilValue:
			*(**bool)(ptr) = nil
		case jsoniter.NumberValue:
			b := iter.ReadInt64() > 0
			*(**bool)(ptr) = &b
		case jsoniter.StringValue:
			b := iter.ReadString() == "1"
			*(**bool)(ptr) = &b
		case jsoniter.BoolValue:
			b := iter.ReadBool()
			*(**bool)(ptr) = &b
		default:
			iter.Skip()
		}
	})

	jsoniter.RegisterFieldEncoderFunc("amogo.CheckboxValue", "Value", func(ptr unsafe.Pointer, stream *jsoniter.Stream) {
		switch p := *(**bool)(unsafe.Pointer(ptr)); p {
		case nil:
			stream.WriteBool(false)
		default:
			stream.WriteBool(*p)
		}
	}, func(pointer unsafe.Pointer) bool { return false })

	//SELECT
	jsoniter.RegisterFieldDecoderFunc("amogo.SelectValue", "Value", func(ptr unsafe.Pointer, iter *jsoniter.Iterator) {
		switch iter.WhatIsNext() {
		case jsoniter.NilValue:
			*(**string)(ptr) = nil
		case jsoniter.StringValue:
			str := iter.ReadString()
			*(**string)(ptr) = &str
		default:
			iter.Skip()
		}
	})

	jsoniter.RegisterFieldDecoderFunc("amogo.SelectValue", "Enum", func(ptr unsafe.Pointer, iter *jsoniter.Iterator) {
		switch iter.WhatIsNext() {
		case jsoniter.NumberValue:
			*(*int64)(ptr) = iter.ReadInt64()
		case jsoniter.StringValue:
			str := iter.ReadString()
			n, err := strconv.ParseInt(str, 10, 64)
			if err == nil {
				*(*int64)(ptr) = n
			}
		default:
			iter.Skip()
		}
	})

	jsoniter.RegisterFieldEncoderFunc("amogo.SelectValue", "Value", func(ptr unsafe.Pointer, stream *jsoniter.Stream) {
		switch p := *(**string)(unsafe.Pointer(ptr)); p {
		case nil:
			stream.WriteString("")
		default:
			stream.WriteString(*p)
		}
	}, func(pointer unsafe.Pointer) bool { return false })

	//MULTISELECT
	jsoniter.RegisterFieldDecoderFunc("amogo.MultiSelectValue", "Value", func(ptr unsafe.Pointer, iter *jsoniter.Iterator) {
		switch iter.WhatIsNext() {
		case jsoniter.NilValue:
			*(**string)(ptr) = nil
		case jsoniter.StringValue:
			str := iter.ReadString()
			*(**string)(ptr) = &str
		default:
			iter.Skip()
		}
	})

	jsoniter.RegisterFieldDecoderFunc("amogo.MultiSelectValue", "Enum", func(ptr unsafe.Pointer, iter *jsoniter.Iterator) {
		switch iter.WhatIsNext() {
		case jsoniter.NumberValue:
			*(*int64)(ptr) = iter.ReadInt64()
		case jsoniter.StringValue:
			str := iter.ReadString()
			n, err := strconv.ParseInt(str, 10, 64)
			if err == nil {
				*(*int64)(ptr) = n
			}
		default:
			iter.Skip()
		}
	})

	jsoniter.RegisterFieldEncoderFunc("amogo.MultiSelectValue", "Value", func(ptr unsafe.Pointer, stream *jsoniter.Stream) {
		switch p := *(**string)(unsafe.Pointer(ptr)); p {
		case nil:
			stream.WriteString("")
		default:
			stream.WriteString(*p)
		}
	}, func(pointer unsafe.Pointer) bool { return false })

	//DATE
	jsoniter.RegisterFieldDecoderFunc("amogo.DateValue", "Value", func(ptr unsafe.Pointer, iter *jsoniter.Iterator) {
		switch iter.WhatIsNext() {
		case jsoniter.NilValue:
			*(**time.Time)(ptr) = nil
		case jsoniter.StringValue:
			str := iter.ReadString()
			if tm, err := time.Parse("2006-01-02 15:04:05", str); err == nil {
				*(**time.Time)(ptr) = &tm
			} else if tm, err := time.Parse("2006-01-02", str); err == nil {
				*(**time.Time)(ptr) = &tm
			} else if rawTm, err := strconv.ParseInt(str, 10, 64); err == nil {
				tm := time.Unix(rawTm, 0).In(time.FixedZone("", 0))
				*(**time.Time)(ptr) = &tm
			} else {
				*(**time.Time)(ptr) = nil
			}
		default:
			iter.Skip()
		}
	})

	jsoniter.RegisterFieldEncoderFunc("amogo.DateValue", "Value", func(ptr unsafe.Pointer, stream *jsoniter.Stream) {
		switch p := *(**time.Time)(unsafe.Pointer(ptr)); p {
		case nil:
			stream.WriteString("")
		default:
			stream.WriteString((*p).Format("2006-01-02"))
		}
	}, func(pointer unsafe.Pointer) bool { return false })

	//URL
	jsoniter.RegisterFieldDecoderFunc("amogo.UrlValue", "Value", func(ptr unsafe.Pointer, iter *jsoniter.Iterator) {
		switch iter.WhatIsNext() {
		case jsoniter.NilValue:
			*(**string)(ptr) = nil
		case jsoniter.StringValue:
			str := iter.ReadString()
			*(**string)(ptr) = &str
		default:
			iter.Skip()
		}
	})

	jsoniter.RegisterFieldEncoderFunc("amogo.UrlValue", "Value", func(ptr unsafe.Pointer, stream *jsoniter.Stream) {
		switch p := *(**string)(unsafe.Pointer(ptr)); p {
		case nil:
			stream.WriteString("")
		default:
			stream.WriteString(*p)
		}
	}, func(pointer unsafe.Pointer) bool { return false })

	//MULTITEXT
	jsoniter.RegisterFieldDecoderFunc("amogo.MultiTextValue", "Value", func(ptr unsafe.Pointer, iter *jsoniter.Iterator) {
		switch iter.WhatIsNext() {
		case jsoniter.NilValue:
			*(**string)(ptr) = nil
		case jsoniter.StringValue:
			str := iter.ReadString()
			*(**string)(ptr) = &str
		default:
			iter.Skip()
		}
	})

	jsoniter.RegisterFieldEncoderFunc("amogo.MultiTextValue", "Value", func(ptr unsafe.Pointer, stream *jsoniter.Stream) {
		switch p := *(**string)(unsafe.Pointer(ptr)); p {
		case nil:
			stream.WriteString("")
		default:
			stream.WriteString(*p)
		}
	}, func(pointer unsafe.Pointer) bool { return false })

	//TEXTAREA
	jsoniter.RegisterFieldDecoderFunc("amogo.TextAreaValue", "Value", func(ptr unsafe.Pointer, iter *jsoniter.Iterator) {
		switch iter.WhatIsNext() {
		case jsoniter.NilValue:
			*(**string)(ptr) = nil
		case jsoniter.StringValue:
			str := iter.ReadString()
			*(**string)(ptr) = &str
		default:
			iter.Skip()
		}
	})

	jsoniter.RegisterFieldEncoderFunc("amogo.TextAreaValue", "Value", func(ptr unsafe.Pointer, stream *jsoniter.Stream) {
		switch p := *(**string)(unsafe.Pointer(ptr)); p {
		case nil:
			stream.WriteString("")
		default:
			stream.WriteString(*p)
		}
	}, func(pointer unsafe.Pointer) bool { return false })

	//RADIOBUTTON
	jsoniter.RegisterFieldDecoderFunc("amogo.RadioButtonValue", "Value", func(ptr unsafe.Pointer, iter *jsoniter.Iterator) {
		switch iter.WhatIsNext() {
		case jsoniter.NilValue:
			*(**string)(ptr) = nil
		case jsoniter.StringValue:
			str := iter.ReadString()
			*(**string)(ptr) = &str
		default:
			iter.Skip()
		}
	})

	jsoniter.RegisterFieldEncoderFunc("amogo.RadioButtonValue", "Value", func(ptr unsafe.Pointer, stream *jsoniter.Stream) {
		switch p := *(**string)(unsafe.Pointer(ptr)); p {
		case nil:
			stream.WriteString("")
		default:
			stream.WriteString(*p)
		}
	}, func(pointer unsafe.Pointer) bool { return false })

	//STREETADDRESS
	jsoniter.RegisterFieldDecoderFunc("amogo.StreetAddressValue", "Value", func(ptr unsafe.Pointer, iter *jsoniter.Iterator) {
		switch iter.WhatIsNext() {
		case jsoniter.NilValue:
			*(**string)(ptr) = nil
		case jsoniter.StringValue:
			str := iter.ReadString()
			*(**string)(ptr) = &str
		default:
			iter.Skip()
		}
	})

	jsoniter.RegisterFieldEncoderFunc("amogo.StreetAddressValue", "Value", func(ptr unsafe.Pointer, stream *jsoniter.Stream) {
		switch p := *(**string)(unsafe.Pointer(ptr)); p {
		case nil:
			stream.WriteString("")
		default:
			stream.WriteString(*p)
		}
	}, func(pointer unsafe.Pointer) bool { return false })

	//SMARTADDRESS
	jsoniter.RegisterFieldDecoderFunc("amogo.SmartAddressValue", "Value", func(ptr unsafe.Pointer, iter *jsoniter.Iterator) {
		switch iter.WhatIsNext() {
		case jsoniter.NilValue:
			*(**string)(ptr) = nil
		case jsoniter.StringValue:
			str := iter.ReadString()
			*(**string)(ptr) = &str
		default:
			iter.Skip()
		}
	})

	jsoniter.RegisterFieldEncoderFunc("amogo.SmartAddressValue", "Value", func(ptr unsafe.Pointer, stream *jsoniter.Stream) {
		switch p := *(**string)(unsafe.Pointer(ptr)); p {
		case nil:
			stream.WriteString("")
		default:
			stream.WriteString(*p)
		}
	}, func(pointer unsafe.Pointer) bool { return false })

	//BIRTHDAY
	jsoniter.RegisterFieldDecoderFunc("amogo.BirthdayValue", "Value", func(ptr unsafe.Pointer, iter *jsoniter.Iterator) {
		switch iter.WhatIsNext() {
		case jsoniter.NilValue:
			*(**time.Time)(ptr) = nil
		case jsoniter.StringValue:
			str := iter.ReadString()
			if tm, err := time.Parse("2006-01-02 15:04:05", str); err == nil {
				*(**time.Time)(ptr) = &tm
			} else if tm, err := time.Parse("2006-01-02", str); err == nil {
				*(**time.Time)(ptr) = &tm
			} else if tm, err := time.Parse("02.01.2006", str); err == nil {
				*(**time.Time)(ptr) = &tm
			} else {
				*(**time.Time)(ptr) = nil
			}
		default:
			iter.Skip()
		}
	})

	jsoniter.RegisterFieldEncoderFunc("amogo.BirthdayValue", "Value", func(ptr unsafe.Pointer, stream *jsoniter.Stream) {
		switch p := *(**time.Time)(unsafe.Pointer(ptr)); p {
		case nil:
			stream.WriteString("")
		default:
			stream.WriteString((*p).Format("2006-01-02"))
		}
	}, func(pointer unsafe.Pointer) bool { return false })

	//LEGAL_ENTITY
	jsoniter.RegisterFieldDecoderFunc("amogo.LegalEntityValue", "Value", func(ptr unsafe.Pointer, iter *jsoniter.Iterator) {
		switch iter.WhatIsNext() {
		case jsoniter.NilValue:
			*(**LegalEntity)(ptr) = nil
		case jsoniter.ObjectValue:
			var legalEntity LegalEntity
			iter.ReadVal(&legalEntity)
			*(**LegalEntity)(ptr) = &legalEntity
		default:
			iter.Skip()
		}
	})

	jsoniter.RegisterFieldEncoderFunc("amogo.LegalEntityValue", "Value", func(ptr unsafe.Pointer, stream *jsoniter.Stream) {
		switch p := *(**LegalEntity)(unsafe.Pointer(ptr)); p {
		case nil:
			stream.WriteVal(&LegalEntity{})
		default:
			stream.WriteVal(*p)
		}
	}, func(pointer unsafe.Pointer) bool { return false })

	//ORG_LEGAL_NAME
	jsoniter.RegisterFieldDecoderFunc("amogo.OrgLegalNameValue", "Value", func(ptr unsafe.Pointer, iter *jsoniter.Iterator) {
		switch iter.WhatIsNext() {
		case jsoniter.NilValue:
			*(**LegalEntity)(ptr) = nil
		case jsoniter.ObjectValue:
			var orgLegalName OrgLegalName
			iter.ReadVal(&orgLegalName)
			*(**OrgLegalName)(ptr) = &orgLegalName
		default:
			iter.Skip()
		}
	})

	jsoniter.RegisterFieldEncoderFunc("amogo.OrgLegalNameValue", "Value", func(ptr unsafe.Pointer, stream *jsoniter.Stream) {
		switch p := *(**OrgLegalName)(unsafe.Pointer(ptr)); p {
		case nil:
			stream.WriteVal(&LegalEntity{})
		default:
			stream.WriteVal(*p)
		}
	}, func(pointer unsafe.Pointer) bool { return false })
	*/
}
