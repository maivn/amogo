package amogo

type Values []Value

type Value struct {
	Value   ValueI `json:"value"`
	Enum    int64  `json:"enum"`
	Subtype string `json:"subtype"`
}

type ValueI interface{}

type LegalEntityValue struct {
	Address                   string `json:"address"`
	EntityType                string `json:"entity_type"`
	ExternalUid               string `json:"external_uid"`
	Kpp                       string `json:"kpp"`
	Name                      string `json:"name"`
	TaxRegistrationReasonCode string `json:"tax_registration_reason_code"`
	VatId                     string `json:"vat_id"`
	Inn                       string `json:"inn"`
}

type Organization struct {
	/*Address                   string `json:"address"`
	City                      string `json:"city"`
	Country                   string `json:"country"`
	EntityType                string `json:"entity_type"`
	ExternalUid               string `json:"external_uid"`
	Kpp                       string `json:"kpp"`
	Line1                     string `json:"line1"`
	Line2                     string `json:"line2"`*/
	Name                      string `json:"name"`
	/*State                     string `json:"state"`
	TaxRegistrationReasonCode string `json:"tax_registration_reason_code"`
	VatId                     string `json:"vat_id"`
	Zip                       string `json:"zip"`*/
}
