package binding

import (
	"net/http"
	"net/url"
	"strings"
)

var FormPost = formPostBinding{}

type formPostBinding struct{}

func (formPostBinding) Name() string {
	return "form-urlencoded"
}

func (formPostBinding) Bind(req *http.Request, obj interface{}) error {
	if err := req.ParseForm(); err != nil {
		return err
	}

	v := prepareValues(req.PostForm)

	if err := mapForm(obj, v); err != nil {
		return err
	}

	return nil
}

func prepareValues(values url.Values) map[string]interface{} {
	m := make(map[string]interface{})
	for key, value := range values {
		ks := keys(strings.Split(strings.ReplaceAll(key, "]", ""), "["))
		var v string
		if len(value) > 0 {
			v = value[0]
		}
		buildMap(&m, ks, v)
	}
	return m
}

type keys []string

func (ks *keys) shift() (string, keys) {
	if len(*ks) > 0 {
		return (*ks)[0], (*ks)[1:]
	}
	return "", []string{}
}

func buildMap(i *map[string]interface{}, keys keys, value string) {
	length := len(keys)
	if length == 0 {
		return
	}

	if length == 1 {
		(*i)[keys[0]] = value
		return
	}

	key, nKeys := keys.shift()
	if (*i)[key] == nil {
		n := make(map[string]interface{})
		(*i)[key] = &n
		buildMap(&n, nKeys, value)
	} else {
		buildMap((*i)[key].(*map[string]interface{}), nKeys, value)
	}
}
