// Copyright 2014 Manu Martinez-Almeida.  All rights reserved.
// Use of this source code is governed by a MIT style
// license that can be found in the LICENSE file.

package binding

import (
	"strings"
	"reflect"
	"strconv"
	"time"
)

type rawKewy string

func (rk *rawKewy) shift() (key, nrk string) {
	a := strings.Split(string(*rk), ".")
	if len(a) > 0 {
		key = a[0]
		nrk = strings.Join(a[1:], ".")
	}
	return
}

type setter map[string][]string

func (s setter) TrySet(value reflect.Value, field reflect.StructField, valuesTag []string) (isSetted bool, err error) {
	return false, nil
}

func mapForm(ptr interface{}, m map[string]interface{}) error {
	return mapFormByTag(ptr, m, "form")
}

func mapFormByTag(ptr interface{}, m map[string]interface{}, tag string) error {
	return mappingByPtr(ptr, m, tag)
}

func mappingByPtr(ptr interface{}, m map[string]interface{}, tag string) error {
	_, err := mapping(reflect.ValueOf(ptr), reflect.StructField{}, &m, tag)
	return err
}

func mapping(value reflect.Value, field reflect.StructField, m interface{}, tag string) (bool, error) {
	if field.Tag.Get(tag) == "-" { // just ignoring this field
		return false, nil
	}
	switch value.Kind() {
	case reflect.Ptr:
		return mapping(value.Elem(), field, m, tag)
	case reflect.Struct:
		tValue := value.Type()
		var isSetted bool
		for i := 0; i < value.NumField(); i++ {
			valueTag := tValue.Field(i).Tag.Get(tag)
			if a, ok := (m).(*map[string]interface{}); ok {
				if nm, has := (*a)[valueTag]; has {
					ok, err := mapping(value.Field(i), tValue.Field(i), nm, tag)
					if err != nil {
						return false, err
					}
					isSetted = isSetted || ok
				}
			}
		}
		return isSetted, nil
	case reflect.Map:
		if a, ok := (m).(*map[string]interface{}); ok {
			mapType := reflect.MapOf(value.Type().Key(), value.Type().Elem())
			mapValue := reflect.MakeMap(mapType)
			for k, v := range *a {
				key := reflect.New(value.Type().Key()).Elem()
				_, err := mapping(key, field, k, tag)
				if err != nil {
					return false, err
				}
				value := reflect.New(value.Type().Elem()).Elem()
				_, err = mapping(value, field, v, tag)
				if err != nil {
					return false, err
				}
				mapValue.SetMapIndex(key, value)
			}
			value.Set(mapValue)
		}

	case reflect.Slice:
		if a, ok := (m).(*map[string]interface{}); ok {
			sliceType := reflect.SliceOf(value.Type().Elem())
			sliceValue := reflect.MakeSlice(sliceType, len(*a), len(*a))
			for k, v := range *a {
				if i, err := strconv.Atoi(k); err == nil {
					_, err := mapping(sliceValue.Index(i), field, v, tag)
					if err != nil {
						return false, err
					}
				}
			}
			value.Set(sliceValue)
		}
	default:
		ok, err := tryToSetValue(value, field, m)
		if err != nil {
			return false, err
		}
		if ok {
			return true, nil
		}
	}

	return false, nil
}

func prepareSlice(values map[string]interface{}) []interface{} {
	var r [] interface{}
	for k, v := range values {
		if _, err := strconv.Atoi(k); err == nil {
			r = append(r, v)
		} else {
			m := make(map[string]interface{})
			m[k] = v
			r = append(r, m)
		}
	}
	return r
}

func tryToSetValue(value reflect.Value, field reflect.StructField, m interface{}) (bool, error) {
	switch value.Kind() {
	case reflect.Int:
		return setIntField(value, 0, m)
	case reflect.Int8:
		return setIntField(value, 8, m)
	case reflect.Int16:
		return setIntField(value, 16, m)
	case reflect.Int32:
		return setIntField(value, 32, m)
	case reflect.Int64:
		switch value.Interface().(type) {
		case time.Duration:
			return setTimeDuration(value, m)
		}
		return setIntField(value, 64, m)

	case reflect.Uint:
		return setUintField(value, 0, m)
	case reflect.Uint8:
		return setUintField(value, 8, m)
	case reflect.Uint16:
		return setUintField(value, 16, m)
	case reflect.Uint32:
		return setUintField(value, 32, m)
	case reflect.Uint64:
		return setUintField(value, 64, m)

	case reflect.Bool:
		return setBoolField(value, m)
	case reflect.Float32:
		return setFloatField(value, 32, m)
	case reflect.Float64:
		return setFloatField(value, 64, m)

	case reflect.String:
		return setStringField(value, m)
	case reflect.Interface:
		return setInterfaceField(value, m)
	}

	return false, nil
}

func setStringField(value reflect.Value, val interface{}) (bool, error) {
	if str, ok := val.(string); ok {
		value.SetString(str)
		return true, nil
	}
	return false, nil
}

func setIntField(value reflect.Value, bitSize int, val interface{}) (bool, error) {
	if str, ok := val.(string); ok {
		if str == "" {
			str = "0"
		}
		intVal, err := strconv.ParseInt(str, 10, bitSize)
		if err == nil {
			value.SetInt(intVal)
			return true, nil
		}
	}
	return false, nil
}

func setUintField(value reflect.Value, bitSize int, val interface{}) (bool, error) {
	if str, ok := val.(string); ok {
		if str == "" {
			str = "0"
		}
		uintVal, err := strconv.ParseUint(str, 10, bitSize)
		if err == nil {
			value.SetUint(uintVal)
			return true, nil
		}
	}
	return false, nil
}

func setBoolField(value reflect.Value, val interface{}) (bool, error) {
	return false, nil
}

func setFloatField(value reflect.Value, bitSize int, val interface{}) (bool, error) {
	return false, nil
}

func setTimeDuration(value reflect.Value, val interface{}) (bool, error) {
	if str, ok := val.(string); ok {
		d, err := time.ParseDuration(str)
		if err != nil {
			return false, err
		}
		value.Set(reflect.ValueOf(d))
		return true, nil
	}
	return false, nil
}

func setInterfaceField(value reflect.Value, val interface{}) (bool, error) {
	var i []interface{}
	if v, ok := val.(*map[string]interface{}); ok {
		for k, v := range *v {
			if _, err := strconv.Atoi(k); err != nil {
				value.Set(reflect.ValueOf(append([]interface{}{}, val)))
				return true, nil
			} else {
				if _, isMap := v.(*map[string]interface{}); isMap {
					i = append(i, v)
				} else {
					mm := make(map[string]interface{})
					mm["value"] = v
					i = append(i, mm)
				}

			}
		}
	}
	value.Set(reflect.ValueOf(i))
	return true, nil
}
