package amogo

import (
	"net/url"
	"github.com/json-iterator/go"
)

type ResponseContacts struct {
	Embedded EmbeddedContacts `json:"_embedded"`
}

type EmbeddedContacts struct {
	Items Contacts `json:"items"`
}

type Contacts []Contact
type MapContacts map[int64]Contact

type Contact struct {
	Id                int64        `json:"id" form:"id"`
	Name              string       `json:"name" form:"name"`
	FirstName         string       `json:"first_name" form:"first_name"`
	LastName          string       `json:"last_name" form:"last_name"`
	ResponsibleUserId int64        `json:"responsible_user_id" form:"responsible_user_id"`
	CreatedAt         int64        `json:"created_at" form:"created_at"`
	CreatedBy         int64        `json:"created_by" form:"created_by"`
	UpdatedAt         int64        `json:"updated_at" form:"updated_at"`
	AccountId         int64        `json:"account_id" form:"account_id"`
	UpdatedBy         int64        `json:"updated_by" form:"updated_by"`
	GroupId           int64        `json:"group_id" form:"group_id"`
	Company           CompanyLink  `json:"company" form:"company"`
	Leads             LeadsLink    `json:"leads" form:"leads"`
	ClosestTaskAt     int64        `json:"closest_task_at" form:"closest_task_at"`
	Tags              Tags         `json:"tags" form:"tags"`
	CustomFields      CustomFields `json:"custom_fields" form:"custom_fields"`
	//Customers         interface{}  `json:"customers" form:"customers"`
}

func (contacts *Contacts) Ids() (ids []int64) {
	for _, contact := range *contacts {
		ids = append(ids, contact.Id)
	}
	return
}

func (contacts *Contacts) GetById(id int64) *Contact {
	for _, contact := range *contacts {
		if contact.Id == id {
			return &contact
		}
	}
	return nil
}

type ContactsLink struct {
	Id    []int64 `json:"id"`
	Links Links   `json:"_links"`
}

func (contactsLink *ContactsLink) Has(contactId int64) bool {
	for _, id := range contactsLink.Id {
		if id == contactId {
			return true
		}
	}
	return false
}

type ContactLink struct {
	Id    int64 `json:"id"`
	Links Links `json:"_links"`
}

func (amo *Amo) ContactList(values url.Values) (contacts Contacts, err error) {
	u := url.URL{
		Scheme:   "https",
		Host:     amo.Domain,
		Path:     "/api/v2/contacts",
		RawQuery: values.Encode(),
	}

	data, err := amo.get(u)
	if err != nil {
		return
	}

	/*if len(data) == 0 {
		err = ErrEntityNotFound
		return
	}*/

	var response ResponseContacts
	err = jsoniter.Unmarshal(data, &response)
	if err != nil {
		return
	}

	contacts = response.Embedded.Items
	return
}

func (amo *Amo) AddContacts(contacts ...Contact) (addContacts Contacts, err error) {
	u := url.URL{
		Scheme: "https",
		Host:   amo.Domain,
		Path:   "/api/v2/contacts",
	}

	data, err := amo.post(u, struct {
		Add Contacts `json:"add"`
	}{contacts})
	if err != nil {
		return
	}

	var response ResponseContacts
	err = jsoniter.Unmarshal(data, &response)
	addContacts = response.Embedded.Items
	return
}

func (amo *Amo) UpdateContacts(contacts ...Contact) (updateContacts Contacts, err error) {
	u := url.URL{
		Scheme: "https",
		Host:   amo.Domain,
		Path:   "/api/v2/contacts",
	}

	data, err := amo.post(u, struct {
		Update Contacts `json:"update"`
	}{contacts})

	if err != nil {
		return
	}

	var response ResponseContacts
	err = jsoniter.Unmarshal(data, &response)
	updateContacts = response.Embedded.Items
	return
}
