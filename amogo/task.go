package amogo

import (
	"net/url"
	"github.com/json-iterator/go"
)

type ResponseTasks struct {
	Embedded EmbeddedTasks `json:"_embedded"`
}

type EmbeddedTasks struct {
	Items Tasks `json:"items"`
}

type Tasks []Task

type Task struct {
	Id int64 `json:"id" form:"id"`
	ElementId         int64 `json:"element_id"`
	ElementType       int64 `json:"element_type"`
	CompleteTillAt    int64  `json:"complete_till_at"`
	TaskType          string `json:"task_type"`
	Text              string `json:"text"`
	CreatedAt         int64  `json:"created_at"`
	UpdatedAt         int64  `json:"updated_at"`
	ResponsibleUserId int64  `json:"responsible_user_id"`
	IsCompleted       bool   `json:"is_completed"`
	CreatedBy         int64  `json:"created_by"`
	AccountId         int64  `json:"account_id"`
	GroupId           int64  `json:"group_id"`
	//result
}

func (tasks *Tasks) Ids() (ids []int64) {
	for _, task := range *tasks {
		ids = append(ids, task.Id)
	}
	return
}

func (tasks *Tasks) GetById(id int64) *Task {
	for _, task := range *tasks {
		if task.Id == id {
			return &task
		}
	}
	return nil
}

func (amo *Amo) TaskList(values url.Values) (tasks Tasks, err error) {
	u := url.URL{
		Scheme:   "https",
		Host:     amo.Domain,
		Path:     "/api/v2/tasks",
		RawQuery: values.Encode(),
	}

	data, err := amo.get(u)
	if err != nil {
		return
	}

	/*if len(data) == 0 {
		err = ErrEntityNotFound
		return
	}*/

	var response ResponseTasks
	err = jsoniter.Unmarshal(data, &response)
	if err != nil {
		return
	}

	tasks = response.Embedded.Items
	return
}

/*
func (amo *Amo) AddContacts(contacts ...Contact) (addContacts Contacts, err error) {
	u := url.URL{
		Scheme: "https",
		Host:   amo.Domain,
		Path:   "/api/v2/contacts",
	}

	data, err := amo.post(u, struct {
		Add Contacts `json:"add"`
	}{contacts})
	if err != nil {
		return
	}

	var response ResponseContacts
	err = jsoniter.Unmarshal(data, &response)
	addContacts = response.Embedded.Items
	return
}

func (amo *Amo) UpdateContacts(contacts ...Contact) (updateContacts Contacts, err error) {
	u := url.URL{
		Scheme: "https",
		Host:   amo.Domain,
		Path:   "/api/v2/contacts",
	}

	data, err := amo.post(u, struct {
		Update Contacts `json:"update"`
	}{contacts})

	if err != nil {
		return
	}

	var response ResponseContacts
	err = jsoniter.Unmarshal(data, &response)
	updateContacts = response.Embedded.Items
	return
}
*/
