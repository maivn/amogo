package amogo

type Events struct {
	Account Account `form:"account"`
	Contacts ContactsActions `form:"contacts"`
	Leads LeadsActions `form:"leads"`
}

type ContactsActions struct {
	Add    MapContacts `form:"add"`
	Update MapContacts `form:"update"`
	Delete MapContacts `form:"delete"`
}

type LeadsActions struct {
	Add    MapLeads `form:"add"`
	Update MapLeads `form:"update"`
	Delete MapLeads `form:"delete"`
	Status MapLeads `form:"status"`
}
