package amogo

import (
	"net/url"
	"github.com/json-iterator/go"
)

type ResponseCustomers struct {
	Embedded EmbeddedCustomers `json:"_embedded"`
}

type EmbeddedCustomers struct {
	Items Customers `json:"items"`
}

type Customers []Customer

type Customer struct {
	Id                int64        `json:"id"`
	Name              string       `json:"name"`
	ResponsibleUserId int64        `json:"responsible_user_id"`
	CreatedBy         int64        `json:"created_by"`
	CreatedAt         int64        `json:"created_at"`
	UpdatedAt         int64        `json:"updated_at"`
	AccountId         int64        `json:"account_id"`
	UpdatedBy         int64        `json:"updated_by"`
	IsDeleted         bool         `json:"is_deleted"`
	MainContact       ContactLink  `json:"main_contact"`
	Tags              Tags         `json:"tags"`
	CustomFields      CustomFields `json:"custom_fields"`
	Contacts          ContactsLink `json:"contacts"`
	Company           CompanyLink  `json:"company"`
	ClosestTaskAt     int64        `json:"closest_task_at"`
	PeriodId          int64        `json:"period_id"`
	PeriodCity        int64        `json:"period_city"`
	NextDate          int64        `json:"next_date"`
}

func (customers *Customers) GetById(id int64) *Customer {
	for _, customer := range *customers {
		if customer.Id == id {
			return &customer
		}
	}
	return nil
}

func (amo *Amo) CustomerList(values url.Values) (customers Customers, err error) {
	u := url.URL{
		Scheme:   "https",
		Host:     amo.Domain,
		Path:     "/api/v2/customers",
		RawQuery: values.Encode(),
	}

	data, err := amo.get(u)
	if err != nil {
		return
	}

	/*if len(data) == 0 {
		err = ErrEntityNotFound
		return
	}*/

	var response ResponseCustomers
	err = jsoniter.Unmarshal(data, &response)
	if err != nil {
		return
	}

	customers = response.Embedded.Items
	return
}

func (amo *Amo) AddCustomers(customers ...Customer) (addCustomers Customers, err error) {
	u := url.URL{
		Scheme: "https",
		Host:   amo.Domain,
		Path:   "/api/v2/customers",
	}

	data, err := amo.post(u, struct {
		Add Customers `json:"add"`
	}{customers})
	if err != nil {
		return
	}

	var response ResponseCustomers
	err = jsoniter.Unmarshal(data, &response)
	addCustomers = response.Embedded.Items
	return
}

func (amo *Amo) UpdateCustomers(customers ...Customer) (updateCustomers Customers, err error) {
	u := url.URL{
		Scheme: "https",
		Host:   amo.Domain,
		Path:   "/api/v2/customers",
	}

	data, err := amo.post(u, struct {
		Update Customers `json:"update"`
	}{customers})
	if err != nil {
		return
	}

	var response ResponseCustomers
	err = jsoniter.Unmarshal(data, &response)
	updateCustomers = response.Embedded.Items
	return
}
