package amogo

import (
	"encoding/json"
	"strconv"
	"time"
)

type CustomFields []CustomField

type CustomField struct {
	Id         int64  `json:"id" form:"id"`
	Name       string `json:"name" form:"name"`
	FieldType  int64  `json:"field_type" form:"field_type"`
	Sort       int64  `json:"sort" form:"sort"`
	Code       string `json:"code" form:"code"`
	IsMultiple bool   `json:"is_multiple" form:"is_multiple"`
	IsSystem   bool   `json:"is_system" form:"is_system"`
	IsEditable bool   `json:"is_editable" form:"is_editable"`
	Enums      Enums  `json:"enums" form:"enums"`
	Values     Values `json:"values" form:"values"`
}

type Enums map[string]string

func (customFields *CustomFields) FillFieldType(mapCustomFields MapCustomFields) {
	for i, customField := range *customFields {
		if cf, ok := mapCustomFields[strconv.FormatInt(customField.Id, 10) ]; ok {
			(*customFields)[i].FieldType = cf.FieldType
		}
	}
}

func (customFields *CustomFields) GetById(id int64) *CustomField {
	for i, customField := range *customFields {
		if customField.Id == id {
			return &(*customFields)[i]
		}
	}
	return nil
}

func (customFields *CustomFields) Merge(mapCustomFields MapCustomFields) {
	for _, mapCustomField := range mapCustomFields {
		if customField := customFields.GetById(mapCustomField.Id); customField != nil {
			customField.Name = mapCustomField.Name
			customField.FieldType = mapCustomField.FieldType
			customField.Sort = mapCustomField.Sort
			customField.Code = mapCustomField.Code
			customField.IsMultiple = mapCustomField.IsMultiple
			customField.IsSystem = mapCustomField.IsSystem
			customField.IsEditable = mapCustomField.IsEditable
			customField.Enums = mapCustomField.Enums
			customField.PrepareValues()
		} else {
			mapCustomField.SetEmptyValues()
			*customFields = append(*customFields, mapCustomField)
		}
	}
}
func (customField *CustomField) PrepareValues() {
	switch customField.FieldType {
	case 6, 14:
		for i, value := range customField.Values {
			if s, ok := value.Value.(string); ok {
				if t, err := time.Parse("2006-01-02 15:04:05", s); err == nil {
					customField.Values[i].Value = t.Format("02.01.2006")
				} else if t, err := time.Parse("02.01.2006", s); err == nil {
					customField.Values[i].Value = t.Format("02.01.2006")
				} else if n, err := strconv.ParseInt(s, 10, 64); err == nil {
					t := time.Unix(n, 0)
					loc, _ := time.LoadLocation("Europe/Moscow")
					customField.Values[i].Value = t.In(loc).Format("02.01.2006")
				}
			}
		}
	case 13:
		for _, subtype := range []string{"address_line_1", "address_line_2", "city", "state", "zip", "country"} {
			if !func(values Values) bool {
				for _, value := range values {
					if value.Subtype == subtype {
						return true
					}
				}
				return false
			}(customField.Values) {
				customField.Values = append(customField.Values, Value{Value: "", Subtype: subtype})
			}
		}
	case 15:
		for i, value := range customField.Values {
			var legal LegalEntityValue
			data, _ := json.Marshal(&value.Value)
			json.Unmarshal(data, &legal)
			if legal.Inn != "" {
				legal.VatId = legal.Inn
			}
			customField.Values[i].Value = legal
		}
	case 17:
		for i, value := range customField.Values {
			var organization Organization
			data, _ := json.Marshal(&value.Value)
			json.Unmarshal(data, &organization)
			customField.Values[i].Value = organization
		}
	}
}

func (customField *CustomField) SetEmptyValues() {
	switch customField.FieldType {
	case 5:
		customField.Values = Values{}
	case 8:
		customField.Values = Values{Value{Enum: func(m map[string]string) int64 {
			for k := range m {
				n, _ := strconv.ParseInt(k, 10, 64)
				return n
			}
			return 0
		}(customField.Enums), Value: ""}}
	case 13:
		customField.Values = Values{
			Value{Value: "", Subtype: "address_line_1"},
			Value{Value: "", Subtype: "address_line_2"},
			Value{Value: "", Subtype: "city"},
			Value{Value: "", Subtype: "state"},
			Value{Value: "", Subtype: "zip"},
			Value{Value: "", Subtype: "country"},
		}
	case 15:
		customField.Values = Values{Value{Value: LegalEntityValue{}}}
	default:
		customField.Values = Values{Value{Value: ""}}
	}
}

func (customFields *CustomFields) Prepare() {
	for i := range *customFields {
		(*customFields)[i].Values.Prepare()
	}
}

func (values *Values) Prepare() {
	var newValues Values
	for _, value := range *values {
		data, err := json.Marshal(value.Value)
		if err != nil {
			return
		}
		var s string
		var l LegalEntityValue
		if err := json.Unmarshal(data, &s); err == nil {
			value.Value = s
			newValues = append(newValues, value)
		} else if err := json.Unmarshal(data, &l); err == nil {
			if l.Address != "" {
				value.Subtype = "address"
				value.Value = l.Address
				newValues = append(newValues, value)
			}
			if l.Name != "" {
				value.Subtype = "name"
				value.Value = l.Name
				newValues = append(newValues, value)
			}
			if l.EntityType != "" {
				value.Subtype = "entity_type"
				value.Value = l.EntityType
				newValues = append(newValues, value)
			}
			if l.ExternalUid != "" {
				value.Subtype = "external_uid"
				value.Value = l.ExternalUid
				newValues = append(newValues, value)
			}
			if l.Kpp != "" {
				value.Subtype = "kpp"
				value.Value = l.Kpp
				newValues = append(newValues, value)
			}
			if l.VatId != "" {
				value.Subtype = "vat_id"
				value.Value = l.VatId
				newValues = append(newValues, value)
			}
			if l.TaxRegistrationReasonCode != "" {
				value.Subtype = "tax_registration_reason_code"
				value.Value = l.TaxRegistrationReasonCode
				newValues = append(newValues, value)
			}
		}
	}
	*values = newValues
}
