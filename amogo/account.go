package amogo

import (
	"net/url"
	"github.com/json-iterator/go"
)

type Account struct {
	Id             int64    `json:"id" form:"id"`
	Name           string   `json:"name" form:"name"`
	Subdomain      string   `json:"subdomain" form:"subdomain"`
	Currency       string   `json:"currency" form:"currency"`
	Timezone       string   `json:"timezone" form:"timezone"`
	TimezoneOffset string   `json:"timezone_offset" form:"timezone_offset"`
	CurrentUser    int64    `json:"current_user" form:"current_user"`
	Embedded       Embedded `json:"_embedded" form:"_embedded"`
}

type Embedded struct {
	Users        MapUsers             `json:"users" form:"users"`
	CustomFields CustomFieldsEntities `json:"custom_fields" form:"custom_fields"`
	NoteTypes    MapNoteTypes         `json:"note_types" form:"note_types"`
	TaskTypes    MapTaskTypes         `json:"task_types" form:"task_types"`
	Pipelines    MapPipelines         `json:"pipelines" form:"pipelines"`
	Groups       MapGroups            `json:"groups" form:"groups"`
}

type MapUsers map[string]User

type User struct {
	Id          int64  `json:"id" form:"id"`
	LastName    string `json:"last_name" form:"last_name"`
	Name        string `json:"name" form:"name"`
	Login       string `json:"login" form:"login"`
	Language    string `json:"language" form:"language"`
	PhoneNumber string `json:"phone_number" form:"phone_number"`
	GroupId     int64  `json:"group_id" form:"group_id"`
	IsActive    bool   `json:"is_active" form:"is_active"`
	IsFree      bool   `json:"is_free" form:"is_free"`
	IsAdmin     bool   `json:"is_admin" form:"is_admin"`
	Rights      Rights `json:"rights" form:"rights"`
}

type Rights struct {
	Mail          string `json:"mail"`
	IncomingLeads string `json:"incoming_leads"`
	Catalogs      string `json:"catalogs"`
	LeadAdd       string `json:"lead_add"`
	LeadView      string `json:"lead_view"`
	LeadEdit      string `json:"lead_edit"`
	LeadDelete    string `json:"lead_delete"`
	LeadExport    string `json:"lead_export"`
	ContactAdd    string `json:"contact_add"`
	ContactView   string `json:"contact_view"`
	ContactEdit   string `json:"contact_edit"`
	ContactDelete string `json:"contact_delete"`
	ContactExport string `json:"contact_export"`
	CompanyAdd    string `json:"company_add"`
	CompanyView   string `json:"company_view"`
	CompanyEdit   string `json:"company_edit"`
	CompanyDelete string `json:"company_delete"`
	CompanyExport string `json:"company_export"`
}

type CustomFieldsEntities struct {
	Contacts  MapCustomFields `json:"contacts"`
	Leads     MapCustomFields `json:"leads"`
	Companies MapCustomFields `json:"companies"`
	Customers MapCustomFields `json:"customers"`
}

type MapCustomFields map[string]CustomField

func (fields *MapCustomFields) MapIdType() (map[int64]int64) {
	m := make(map[int64]int64)
	for _, field := range *fields {
		m[field.Id] = field.FieldType
	}
	return m
}

func (customFieldsEntities *CustomFieldsEntities) GetTypeCustomFieldById(id int64) (CustomField) {
	for _, cf := range customFieldsEntities.Contacts {
		if cf.Id == id {
			return cf
		}
	}
	for _, cf := range customFieldsEntities.Leads {
		if cf.Id == id {
			return cf
		}
	}
	for _, cf := range customFieldsEntities.Companies {
		if cf.Id == id {
			return cf
		}
	}
	for _, cf := range customFieldsEntities.Customers {
		if cf.Id == id {
			return cf
		}
	}
	return CustomField{}
}

type MapNoteTypes map[string]NoteType

type NoteType struct {
	Id       int64  `json:"id"`
	Name     string `json:"name"`
	Code     string `json:"code"`
	Editable bool   `json:"editable"`
}

type MapTaskTypes map[string]TaskType

type TaskType struct {
	Id   int64  `json:"id"`
	Name string `json:"name"`
}

type MapPipelines map[string]Pipeline

type Pipeline struct {
	Id       int64       `json:"id"`
	Name     string      `json:"name"`
	Sort     int64       `json:"sort"`
	IsMain   bool        `json:"is_main"`
	Statuses MapStatuses `json:"statuses"`
}

type PipelineLink struct {
	Id    int64 `json:"id"`
	Links Links `json:"_links"`
}

type MapStatuses map[string]Status

type Status struct {
	Id       int64  `json:"id"`
	Name     string `json:"name"`
	Sort     int64  `json:"sort"`
	Color    string  `json:"color"`
	Editable bool   `json:"editable"`
}

type MapGroups map[string]Group

type Group struct {
	Id   int64  `json:"id"`
	Name string `json:"name"`
}

func (amo *Amo) account() (err error) {
	u := url.URL{
		Scheme: "https",
		Host:   amo.Domain,
		Path:   "/api/v2/account",
		RawQuery: url.Values{
			"with": {"custom_fields,users,pipelines,groups,note_types,task_types"},
			"inactive_users": {"Y"},
		}.Encode(),
	}

	data, err := amo.get(u)
	if err != nil {
		return
	}

	err = jsoniter.Unmarshal(data, &amo.Account)
	return
}
