package amogo

import (
	"net/url"
	"io/ioutil"
	"net/http"
	"bytes"
	"github.com/json-iterator/go"
	"fmt"
	"strings"
)

func (amo *Amo) postForm(u url.URL, values url.Values) (data []byte, err error) {
	var resp *http.Response
	req, _ := http.NewRequest("POST", u.String(), strings.NewReader(values.Encode()))
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	if amo.Token != "" {
		req.Header.Add("Authorization", "Bearer "+amo.Token)
	}
	//resp, err = amo.Client.PostForm(u.String(), values)
	resp, err = amo.Client.Do(req)
	if err != nil {
		return
	}
	defer resp.Body.Close()
	data, err = ioutil.ReadAll(resp.Body)

	if len(data) == 0 {
		err = ErrEntityNotFound
		return
	}

	var responseError Error
	err = jsoniter.Unmarshal(data, &responseError)
	if err != nil {
		return
	}

	if responseError.Response.ErrorCode != 0 {
		err = &responseError.Response
		return
	}
	return
}

func (amo *Amo) post(u url.URL, entity interface{}) (data []byte, err error) {
	jsonEntity, err := jsoniter.Marshal(entity)
	if err != nil {
		return
	}

	var resp *http.Response
	req, _ := http.NewRequest("POST", u.String(), bytes.NewReader(jsonEntity))
	if amo.Token != "" {
		req.Header.Add("Authorization", "Bearer "+amo.Token)
	}
	resp, err = amo.Client.Do(req)
	//resp, err = amo.Client.Post(u.String(), "application/json", bytes.NewReader(jsonEntity))
	if err != nil {
		return
	}
	defer resp.Body.Close()
	data, err = ioutil.ReadAll(resp.Body)

	fmt.Println("post", string(data))
	if len(data) == 0 {
		err = ErrEntityNotFound
		return
	}

	var responseError ResponseError
	err = jsoniter.Unmarshal(data, &responseError)
	if err != nil {
		return
	}

	if responseError.ErrorCode != 0 {
		err = &responseError
		return
	}
	return
}

func (amo *Amo) get(u url.URL) (data []byte, err error) {
	var resp *http.Response
	req, _ := http.NewRequest("GET", u.String(), nil)
	if amo.Token != "" {
		req.Header.Add("Authorization", "Bearer "+amo.Token)
	}
	resp, err = amo.Client.Do(req)
	//	resp, err = amo.Client.Get(u.String())
	if err != nil {
		return
	}
	defer resp.Body.Close()
	data, err = ioutil.ReadAll(resp.Body)

	if len(data) == 0 {
		err = ErrEntityNotFound
		return
	}

	var responseError ResponseError
	err = jsoniter.Unmarshal(data, &responseError)
	if err != nil {
		return
	}

	if responseError.ErrorCode != 0 {
		err = &responseError
		return
	}
	return
}
