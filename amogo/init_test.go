package amogo

import (
	"testing"
	"github.com/json-iterator/go"
	"time"
)

func TestEncodeText(t *testing.T) {
	text := TextValue{nil}
	data, _ := jsoniter.Marshal(&text)
	if string(data) != "{\"value\":\"\"}" {
		t.Error("Error TestEncodeNilText")
	}

	str := ""
	text = TextValue{&str}
	data, _ = jsoniter.Marshal(&text)
	if string(data) != "{\"value\":\"\"}" {
		t.Error("Error TestEncodeEmptyText")
	}

	str = "text"
	text = TextValue{&str}
	data, _ = jsoniter.Marshal(&text)
	if string(data) != "{\"value\":\"text\"}" {
		t.Error("Error TestEncodeText")
	}
}

func TestDecodeText(t *testing.T) {
	var value TextValue
	data := []byte("{\"value\":null")

	jsoniter.Unmarshal(data, &value)
	if value.Value != nil {
		t.Error("Error TestDecodeNilText")
	}

	data = []byte("{\"value\":\"\"}")

	jsoniter.Unmarshal(data, &value)
	if *value.Value != "" {
		t.Error("Error TestDecodeEmptyText")
	}

	data = []byte("{\"value\":\"text\"}")

	jsoniter.Unmarshal(data, &value)
	if *value.Value != "text" {
		t.Error("Error TestDecodeText")
	}
}

func TestEncoderNumeric(t *testing.T) {
	text := NumericValue{nil}
	data, _ := jsoniter.Marshal(&text)
	if string(data) != "{\"value\":\"\"}" {
		t.Error("Error TestEncoderNilNumeric")
	}

	n := int64(0)
	text = NumericValue{&n}
	data, _ = jsoniter.Marshal(&text)
	if string(data) != "{\"value\":0}" {
		t.Error("Error TestEncoder0Numeric")
	}

	n = int64(10)
	text = NumericValue{&n}
	data, _ = jsoniter.Marshal(&text)
	if string(data) != "{\"value\":10}" {
		t.Error("Error TestEncoderNumeric")
	}
}

func TestDecoderNumeric(t *testing.T) {
	var value NumericValue
	data := []byte("{\"value\":null")

	jsoniter.Unmarshal(data, &value)
	if value.Value != nil {
		t.Error("Error TestDecoderNilNumeric")
	}

	data = []byte("{\"value\":0}")

	jsoniter.Unmarshal(data, &value)
	if *value.Value != 0 {
		t.Error("Error TestDecoder0Numeric")
	}

	data = []byte("{\"value\":10}")

	jsoniter.Unmarshal(data, &value)
	if *value.Value != 10 {
		t.Error("Error TestDecoder10Numeric")
	}

	data = []byte("{\"value\":\"0\"}")

	jsoniter.Unmarshal(data, &value)
	if *value.Value != 0 {
		t.Error("Error TestDecoderText0Numeric")
	}

	data = []byte("{\"value\":\"10\"}")

	jsoniter.Unmarshal(data, &value)
	if *value.Value != 10 {
		t.Error("Error TestDecoderText10Numeric")
	}

}

func TestEncoderCheckbox(t *testing.T) {
	text := CheckboxValue{nil}
	data, _ := jsoniter.Marshal(&text)
	if string(data) != "{\"value\":false}" {
		t.Error("Error TestEncoderNilCheckbox")
	}

	n := false
	text = CheckboxValue{&n}
	data, _ = jsoniter.Marshal(&text)
	if string(data) != "{\"value\":false}" {
		t.Error("Error TestEncoderFalseCheckbox")
	}

	n = true
	text = CheckboxValue{&n}
	data, _ = jsoniter.Marshal(&text)
	if string(data) != "{\"value\":true}" {
		t.Error("Error TestEncoderTrueCheckbox")
	}
}

func TestDecoderCheckbox(t *testing.T) {
	var value CheckboxValue
	data := []byte("{\"value\":null}")

	jsoniter.Unmarshal(data, &value)
	if value.Value != nil {
		t.Error("Error TestDecoderNilNumeric")
	}

	data = []byte("{\"value\":0}")

	jsoniter.Unmarshal(data, &value)
	if *value.Value != false {
		t.Error("Error TestDecoder0Numeric")
	}

	data = []byte("{\"value\":10}")

	jsoniter.Unmarshal(data, &value)
	if *value.Value != true {
		t.Error("Error TestDecoder10Numeric")
	}

	data = []byte("{\"value\":\"0\"}")

	jsoniter.Unmarshal(data, &value)
	if *value.Value != false {
		t.Error("Error TestDecoderText0Numeric")
	}

	data = []byte("{\"value\":\"10\"}")

	jsoniter.Unmarshal(data, &value)
	if *value.Value != false {
		t.Error("Error TestDecoderText10Numeric")
	}

	data = []byte("{\"value\":\"true\"}")

	jsoniter.Unmarshal(data, &value)
	if *value.Value != true {
		t.Error("Error TestDecoderText10Numeric")
	}

	data = []byte("{\"value\":\"true\"}")

	jsoniter.Unmarshal(data, &value)
	if *value.Value != true {
		t.Error("Error TestDecoderText10Numeric")
	}

}

func TestEncodeSelect(t *testing.T) {
	text := SelectValue{nil, 0}
	data, _ := jsoniter.Marshal(&text)
	if string(data) != "{\"value\":\"\",\"enum\":0}" {
		t.Error("Error TestEncodeNilText")
	}

	str := ""
	text = SelectValue{&str, 10}
	data, _ = jsoniter.Marshal(&text)
	if string(data) != "{\"value\":\"\",\"enum\":10}" {
		t.Error("Error TestEncodeEmptyText")
	}

	str = "text"
	text = SelectValue{&str, 0}
	data, _ = jsoniter.Marshal(&text)
	if string(data) != "{\"value\":\"text\",\"enum\":0}" {
		t.Error("Error TestEncodeText")
	}
}

func TestDecodeSelect(t *testing.T) {
	var value SelectValue
	data := []byte("{\"value\":null")

	jsoniter.Unmarshal(data, &value)
	if value.Value != nil {
		t.Error("Error TestDecodeNilText")
	}

	data = []byte("{\"value\":\"\",\"enum\":10}")

	jsoniter.Unmarshal(data, &value)
	if *value.Value != "" {
		t.Error("Error TestDecodeEmptyText")
	}

	data = []byte("{\"value\":\"text\"}")

	jsoniter.Unmarshal(data, &value)
	if *value.Value != "text" {
		t.Error("Error TestDecodeText")
	}

	data = []byte("{\"value\":\"text\",\"enum\":0}")

	jsoniter.Unmarshal(data, &value)

	if *value.Value != "text" {
		t.Error("Error TestDecodeText")
	}
}

func TestEncodeMultiSelect(t *testing.T) {
	txt := "test"
	arr := MultiSelectValues{MultiSelectValue{nil, 0}, MultiSelectValue{&txt, 0}}
	data, _ := jsoniter.Marshal(&arr)
	if string(data) != "[{\"value\":\"\",\"enum\":0},{\"value\":\"test\",\"enum\":0}]" {
		t.Error("Error TestEncodeNilText")
	}

	str := ""
	text := MultiSelectValue{&str, 10}
	data, _ = jsoniter.Marshal(&text)
	if string(data) != "{\"value\":\"\",\"enum\":10}" {
		t.Error("Error TestEncodeEmptyText")
	}

	str = "text"
	text = MultiSelectValue{&str, 0}
	data, _ = jsoniter.Marshal(&text)
	if string(data) != "{\"value\":\"text\",\"enum\":0}" {
		t.Error("Error TestEncodeText")
	}
}

func TestDecodeDate(t *testing.T) {
	var value DateValue
	data := []byte("{\"value\":null")

	jsoniter.Unmarshal(data, &value)
	if value.Value != nil {
		t.Error("Error TestDecodeDate")
	}

	data = []byte("{\"value\":\"\"}")

	jsoniter.Unmarshal(data, &value)
	if value.Value != nil {
		t.Error("Error TestDecodeDate")
	}

	data = []byte("{\"value\":\"text\"}")

	jsoniter.Unmarshal(data, &value)
	if value.Value != nil {
		t.Error("Error TestDecodeDate")
	}

	data = []byte("{\"value\":\"2019-10-02 10:04:05\"}")

	jsoniter.Unmarshal(data, &value)
	tm, _ := time.Parse("2006-01-02 15:04:05", "2019-10-02 10:04:05")
	if *value.Value != tm {
		t.Error("Error TestDecodeText")
	}
}

func TestEncodeDate(t *testing.T) {
	text := DateValue{nil}
	data, _ := jsoniter.Marshal(&text)
	if string(data) != "{\"value\":\"\"}" {
		t.Error("Error TestEncodeNilText")
	}

	tm, _ := time.Parse("2006-01-02 15:04:05", "2019-10-02 10:04:05")
	text = DateValue{&tm}
	data, _ = jsoniter.Marshal(&text)
	if string(data) != "{\"value\":\"2019-10-02 10:04:05\"}" {
		t.Error("Error TestEncodeEmptyText")
	}
}

func TestEncodeUrl(t *testing.T) {
	text := UrlValue{nil}
	data, _ := jsoniter.Marshal(&text)
	if string(data) != "{\"value\":\"\"}" {
		t.Error("Error TestEncodeNilText")
	}

	str := ""
	text = UrlValue{&str}
	data, _ = jsoniter.Marshal(&text)
	if string(data) != "{\"value\":\"\"}" {
		t.Error("Error TestEncodeEmptyText")
	}

	str = "text"
	text = UrlValue{&str}
	data, _ = jsoniter.Marshal(&text)
	if string(data) != "{\"value\":\"text\"}" {
		t.Error("Error TestEncodeText")
	}
}

func TestDecodeUrl(t *testing.T) {
	var value UrlValue
	data := []byte("{\"value\":null")

	jsoniter.Unmarshal(data, &value)
	if value.Value != nil {
		t.Error("Error TestDecodeNilText")
	}

	data = []byte("{\"value\":\"\"}")

	jsoniter.Unmarshal(data, &value)
	if *value.Value != "" {
		t.Error("Error TestDecodeEmptyText")
	}

	data = []byte("{\"value\":\"text\"}")

	jsoniter.Unmarshal(data, &value)
	if *value.Value != "text" {
		t.Error("Error TestDecodeText")
	}
}

func TestEncodeMultiText(t *testing.T) {
	text := MultiTextValue{nil, 0}
	data, _ := jsoniter.Marshal(&text)
	if string(data) != "{\"value\":\"\",\"enum\":0}" {
		t.Error("Error TestEncodeNilText")
	}

	str := ""
	text = MultiTextValue{&str, 10}
	data, _ = jsoniter.Marshal(&text)
	if string(data) != "{\"value\":\"\",\"enum\":10}" {
		t.Error("Error TestEncodeEmptyText")
	}

	str = "text"
	text = MultiTextValue{&str, 0}
	data, _ = jsoniter.Marshal(&text)
	if string(data) != "{\"value\":\"text\",\"enum\":0}" {
		t.Error("Error TestEncodeText")
	}
}

func TestDecodeMultiText(t *testing.T) {
	var value MultiTextValue
	data := []byte("{\"value\":null")

	jsoniter.Unmarshal(data, &value)
	if value.Value != nil {
		t.Error("Error TestDecodeNilText")
	}

	data = []byte("{\"value\":\"\",\"enum\":10}")

	jsoniter.Unmarshal(data, &value)
	if *value.Value != "" {
		t.Error("Error TestDecodeEmptyText")
	}

	data = []byte("{\"value\":\"text\"}")

	jsoniter.Unmarshal(data, &value)
	if *value.Value != "text" {
		t.Error("Error TestDecodeText")
	}

	data = []byte("{\"value\":\"text\",\"enum\":0}")

	jsoniter.Unmarshal(data, &value)

	if *value.Value != "text" {
		t.Error("Error TestDecodeText")
	}
}

func TestEncodeTextArea(t *testing.T) {
	text := TextAreaValue{nil}
	data, _ := jsoniter.Marshal(&text)
	if string(data) != "{\"value\":\"\"}" {
		t.Error("Error TestEncodeNilText")
	}

	str := ""
	text = TextAreaValue{&str}
	data, _ = jsoniter.Marshal(&text)
	if string(data) != "{\"value\":\"\"}" {
		t.Error("Error TestEncodeEmptyText")
	}

	str = "text"
	text = TextAreaValue{&str}
	data, _ = jsoniter.Marshal(&text)
	if string(data) != "{\"value\":\"text\"}" {
		t.Error("Error TestEncodeText")
	}
}

func TestDecodeTextArea(t *testing.T) {
	var value TextAreaValue
	data := []byte("{\"value\":null")

	jsoniter.Unmarshal(data, &value)
	if value.Value != nil {
		t.Error("Error TestDecodeNilText")
	}

	data = []byte("{\"value\":\"\"}")

	jsoniter.Unmarshal(data, &value)
	if *value.Value != "" {
		t.Error("Error TestDecodeEmptyText")
	}

	data = []byte("{\"value\":\"text\"}")

	jsoniter.Unmarshal(data, &value)
	if *value.Value != "text" {
		t.Error("Error TestDecodeText")
	}
}

func TestEncodeRadioButton(t *testing.T) {
	text := RadioButtonValue{nil}
	data, _ := jsoniter.Marshal(&text)
	if string(data) != "{\"value\":\"\"}" {
		t.Error("Error TestEncodeNilText")
	}

	str := ""
	text = RadioButtonValue{&str}
	data, _ = jsoniter.Marshal(&text)
	if string(data) != "{\"value\":\"\"}" {
		t.Error("Error TestEncodeEmptyText")
	}

	str = "text"
	text = RadioButtonValue{&str}
	data, _ = jsoniter.Marshal(&text)
	if string(data) != "{\"value\":\"text\"}" {
		t.Error("Error TestEncodeText")
	}
}

func TestDecodeRadioButton(t *testing.T) {
	var value RadioButtonValue
	data := []byte("{\"value\":null")

	jsoniter.Unmarshal(data, &value)
	if value.Value != nil {
		t.Error("Error TestDecodeNilText")
	}

	data = []byte("{\"value\":\"\"}")

	jsoniter.Unmarshal(data, &value)
	if *value.Value != "" {
		t.Error("Error TestDecodeEmptyText")
	}

	data = []byte("{\"value\":\"text\"}")

	jsoniter.Unmarshal(data, &value)
	if *value.Value != "text" {
		t.Error("Error TestDecodeText")
	}
}

func TestEncodeStreetAddress(t *testing.T) {
	text := StreetAddressValue{nil}
	data, _ := jsoniter.Marshal(&text)
	if string(data) != "{\"value\":\"\"}" {
		t.Error("Error TestEncodeNilText")
	}

	str := ""
	text = StreetAddressValue{&str}
	data, _ = jsoniter.Marshal(&text)
	if string(data) != "{\"value\":\"\"}" {
		t.Error("Error TestEncodeEmptyText")
	}

	str = "text"
	text = StreetAddressValue{&str}
	data, _ = jsoniter.Marshal(&text)
	if string(data) != "{\"value\":\"text\"}" {
		t.Error("Error TestEncodeText")
	}
}

func TestDecodeStreetAddress(t *testing.T) {
	var value StreetAddressValue
	data := []byte("{\"value\":null")

	jsoniter.Unmarshal(data, &value)
	if value.Value != nil {
		t.Error("Error TestDecodeNilText")
	}

	data = []byte("{\"value\":\"\"}")

	jsoniter.Unmarshal(data, &value)
	if *value.Value != "" {
		t.Error("Error TestDecodeEmptyText")
	}

	data = []byte("{\"value\":\"text\"}")

	jsoniter.Unmarshal(data, &value)
	if *value.Value != "text" {
		t.Error("Error TestDecodeText")
	}
}

func TestEncodeSmartAddress(t *testing.T) {
	text := SmartAddressValue{nil, "city"}
	data, _ := jsoniter.Marshal(&text)
	if string(data) != "{\"value\":\"\",\"subtype\":\"city\"}" {
		t.Error("Error TestEncodeNilText")
	}

	str := ""
	text = SmartAddressValue{&str, "city"}
	data, _ = jsoniter.Marshal(&text)
	if string(data) != "{\"value\":\"\",\"subtype\":\"city\"}" {
		t.Error("Error TestEncodeEmptyText")
	}

	str = "text"
	text = SmartAddressValue{&str, "city"}
	data, _ = jsoniter.Marshal(&text)
	if string(data) != "{\"value\":\"text\",\"subtype\":\"city\"}" {
		t.Error("Error TestEncodeText")
	}
}

func TestDecodeSmartAddress(t *testing.T) {
	var value SmartAddressValue
	data := []byte("{\"value\":null")

	jsoniter.Unmarshal(data, &value)
	if value.Value != nil {
		t.Error("Error TestDecodeNilText")
	}

	data = []byte("{\"value\":\"\"}")

	jsoniter.Unmarshal(data, &value)
	if *value.Value != "" {
		t.Error("Error TestDecodeEmptyText")
	}

	data = []byte("{\"value\":\"text\"}")

	jsoniter.Unmarshal(data, &value)
	if *value.Value != "text" {
		t.Error("Error TestDecodeText")
	}
}

func TestDecodeBirthDay(t *testing.T) {
	var value BirthdayValue
	data := []byte("{\"value\":null")

	jsoniter.Unmarshal(data, &value)
	if value.Value != nil {
		t.Error("Error TestDecodeDate")
	}

	data = []byte("{\"value\":\"\"}")

	jsoniter.Unmarshal(data, &value)
	if value.Value != nil {
		t.Error("Error TestDecodeDate")
	}

	data = []byte("{\"value\":\"text\"}")

	jsoniter.Unmarshal(data, &value)
	if value.Value != nil {
		t.Error("Error TestDecodeDate")
	}

	data = []byte("{\"value\":\"2019-10-02 10:04:05\"}")

	jsoniter.Unmarshal(data, &value)
	tm, _ := time.Parse("2006-01-02 15:04:05", "2019-10-02 10:04:05")
	if *value.Value != tm {
		t.Error("Error TestDecodeText")
	}
}

func TestEncodeBirthDay(t *testing.T) {
	text := BirthdayValue{nil}
	data, _ := jsoniter.Marshal(&text)
	if string(data) != "{\"value\":\"\"}" {
		t.Error("Error TestEncodeNilText")
	}

	tm, _ := time.Parse("2006-01-02 15:04:05", "2019-10-02 10:04:05")
	text = BirthdayValue{&tm}
	data, _ = jsoniter.Marshal(&text)
	if string(data) != "{\"value\":\"2019-10-02 10:04:05\"}" {
		t.Error("Error TestEncodeEmptyText")
	}
}

func TestDecodeLegalEntity(t *testing.T) {
	var value LegalEntityValue
	data := []byte("{\"value\":null")

	jsoniter.Unmarshal(data, &value)
	if value.Value != nil {
		t.Error("Error TestDecodeDate")
	}

	data = []byte("{\"value\":\"\"}")

	jsoniter.Unmarshal(data, &value)
	if value.Value != nil {
		t.Error("Error TestDecodeDate")
	}

	data = []byte("{\"value\":\"text\"}")

	jsoniter.Unmarshal(data, &value)
	if value.Value != nil {
		t.Error("Error TestDecodeDate")
	}

	data = []byte("{\"value\":{\"address\":\"kirov\"}}")

	jsoniter.Unmarshal(data, &value)
	if value.Value.Address != "kirov" {
		t.Error("Error TestDecodeDate")
	}
}

func TestEncodeLegalEntity(t *testing.T) {
	text := LegalEntityValue{nil}
	data, _ := jsoniter.Marshal(&text)
	if string(data) != "{\"value\":{\"address\":\"\",\"entity_type\":\"\",\"external_uid\":\"\",\"kpp\":\"\",\"name\":\"\",\"tax_registration_reason_code\":\"\",\"vat_id\":\"\"}}" {
		t.Error("Error TestEncodeNilText")
	}

	text = LegalEntityValue{&LegalEntity{}}
	data, _ = jsoniter.Marshal(&text)
	if string(data) != "{\"value\":{\"address\":\"\",\"entity_type\":\"\",\"external_uid\":\"\",\"kpp\":\"\",\"name\":\"\",\"tax_registration_reason_code\":\"\",\"vat_id\":\"\"}}" {
		t.Error("Error TestEncodeNilText!!!!!!!!")
	}

	text = LegalEntityValue{&LegalEntity{VatId: "123"}}
	data, _ = jsoniter.Marshal(&text)
	if string(data) != "{\"value\":{\"address\":\"\",\"entity_type\":\"\",\"external_uid\":\"\",\"kpp\":\"\",\"name\":\"\",\"tax_registration_reason_code\":\"\",\"vat_id\":\"123\"}}" {
		t.Error("Error TestEncodeNilText!!!!!!!!")
	}
}

func TestDecodeOrgLegalNameValue(t *testing.T) {
	var value OrgLegalNameValue
	data := []byte("{\"value\":null")

	jsoniter.Unmarshal(data, &value)
	if value.Value != nil {
		t.Error("Error TestDecodeDate")
	}

	data = []byte("{\"value\":\"\"}")

	jsoniter.Unmarshal(data, &value)
	if value.Value != nil {
		t.Error("Error TestDecodeDate")
	}

	data = []byte("{\"value\":\"text\"}")

	jsoniter.Unmarshal(data, &value)
	if value.Value != nil {
		t.Error("Error TestDecodeDate")
	}

	data = []byte("{\"value\":{\"address\":\"kirov\"}}")

	jsoniter.Unmarshal(data, &value)
	if value.Value.Address != "kirov" {
		t.Error("Error TestDecodeDate")
	}
}

func TestEncodeOrgLegalNameValue(t *testing.T) {
	text := OrgLegalNameValue{nil}
	data, _ := jsoniter.Marshal(&text)
	if string(data) != "{\"value\":{\"address\":\"\",\"entity_type\":\"\",\"external_uid\":\"\",\"kpp\":\"\",\"name\":\"\",\"tax_registration_reason_code\":\"\",\"vat_id\":\"\"}}" {
		t.Error("Error TestEncodeNilText")
	}

	text = OrgLegalNameValue{&OrgLegalName{}}
	data, _ = jsoniter.Marshal(&text)
	if string(data) != "{\"value\":{\"address\":\"\",\"city\":\"\",\"country\":\"\",\"entity_type\":\"\",\"external_uid\":\"\",\"kpp\":\"\",\"line1\":\"\",\"line2\":\"\",\"name\":\"\",\"state\":\"\",\"tax_registration_reason_code\":\"\",\"vat_id\":\"\",\"zip\":\"\"}}" {
		t.Error("Error TestEncodeNilText!!!!!!!!")
	}

	text = OrgLegalNameValue{&OrgLegalName{VatId: "123"}}
	data, _ = jsoniter.Marshal(&text)
	if string(data) != "{\"value\":{\"address\":\"\",\"city\":\"\",\"country\":\"\",\"entity_type\":\"\",\"external_uid\":\"\",\"kpp\":\"\",\"line1\":\"\",\"line2\":\"\",\"name\":\"\",\"state\":\"\",\"tax_registration_reason_code\":\"\",\"vat_id\":\"123\",\"zip\":\"\"}}" {
		t.Error("Error TestEncodeNilText!!!!!!!!")
	}
}
