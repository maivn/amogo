package amogo

import (
	"net/url"
	"github.com/json-iterator/go"
)

type ResponseLeads struct {
	Embedded EmbeddedLeads `json:"_embedded"`
}

type EmbeddedLeads struct {
	Items Leads `json:"items"`
}

type Leads []Lead
type MapLeads map[int64]Lead

type Lead struct {
	Id                int64        `json:"id" form:"id"`
	Name              string       `json:"name" form:"name"`
	ResponsibleUserId int64        `json:"responsible_user_id" form:"responsible_user_id"`
	CreatedBy         int64        `json:"created_by" form:"created_by"`
	CreatedAt         int64        `json:"created_at" form:"created_at"`
	UpdatedAt         int64        `json:"updated_at" form:"updated_at"`
	AccountId         int64        `json:"account_id" form:"account_id"`
	UpdatedBy         int64        `json:"updated_by" form:"updated_by"`
	IsDeleted         bool         `json:"is_deleted" form:"is_deleted"`
	MainContact       ContactLink  `json:"main_contact" form:"main_contact"`
	GroupId           int64        `json:"group_id" form:"group_id"`
	Company           CompanyLink  `json:"company" form:"company"`
	ClosestTaskAt     int64        `json:"closest_task_at" form:"closest_task_at"`
	Tags              Tags         `json:"tags" form:"tags"`
	CustomFields      CustomFields `json:"custom_fields" form:"custom_fields"`
	Contacts          ContactsLink `json:"contacts" form:"contacts"`
	StatusId          int64        `json:"status_id" form:"status_id"`
	Sale              int64        `json:"sale" form:"sale"`
	Pipeline          PipelineLink `json:"pipeline" form:"pipeline"`
}

func (leads *Leads) Ids() (ids []int64) {
	for _, lead := range *leads {
		ids = append(ids, lead.Id)
	}
	return
}

func (leads *Leads) GetById(id int64) *Lead {
	for _, lead := range *leads {
		if lead.Id == id {
			return &lead
		}
	}
	return nil
}

type LeadsLink struct {
	Id    []int64 `json:"id"`
	Links Links   `json:"_links"`
}

func (leadsLink *LeadsLink) Has(leadId int64) bool {
	for _, id := range leadsLink.Id {
		if id == leadId {
			return true
		}
	}
	return false
}

func (amo *Amo) LeadList(values url.Values) (leads Leads, err error) {
	u := url.URL{
		Scheme:   "https",
		Host:     amo.Domain,
		Path:     "/api/v2/leads",
		RawQuery: values.Encode(),
	}

	data, err := amo.get(u)
	if err != nil {
		return
	}

	/*if len(data) == 0 {
		err = ErrEntityNotFound
		return
	}*/

	var response ResponseLeads
	err = jsoniter.Unmarshal(data, &response)
	if err != nil {
		return
	}

	leads = response.Embedded.Items
	return
}

func (amo *Amo) AddLeads(leads ...Lead) (addLeads Leads, err error) {
	u := url.URL{
		Scheme: "https",
		Host:   amo.Domain,
		Path:   "/api/v2/leads",
	}

	data, err := amo.post(u, struct {
		Add Leads `json:"add"`
	}{leads})
	if err != nil {
		return
	}

	var response ResponseLeads
	err = jsoniter.Unmarshal(data, &response)
	addLeads = response.Embedded.Items
	return
}

func (amo *Amo) UpdateLeads(leads ...Lead) (updateLeads Leads, err error) {
	u := url.URL{
		Scheme: "https",
		Host:   amo.Domain,
		Path:   "/api/v2/leads",
	}

	data, err := amo.post(u, struct {
		Update Leads `json:"update"`
	}{leads})
	if err != nil {
		return
	}

	var response ResponseLeads
	err = jsoniter.Unmarshal(data, &response)
	updateLeads = response.Embedded.Items
	return
}
