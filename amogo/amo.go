package amogo

import (
	"net/http"
	"strings"
	"fmt"
	"net/http/cookiejar"
	"golang.org/x/net/publicsuffix"
)

type Amo struct {
	Config
	Token string
	Account Account
	Client  *http.Client
}

type Config struct {
	Domain string
	Login  string
	Hash   string
}

func New(domain, login, hash string) (*Amo, error) {
	var amo Amo
	if strings.Index(domain, ".amocrm.ru") == - 1 {
		domain = fmt.Sprintf("%s.amocrm.ru", domain)
	}
	amo.Domain = domain
	amo.Login = login
	amo.Hash = hash

	jar, err := cookiejar.New(&cookiejar.Options{PublicSuffixList: publicsuffix.List})
	if err != nil {
		return &amo, err
	}

	amo.Client = &http.Client{
		Jar: jar,
	}

	err = amo.auth()
	if err != nil {
		return &amo, err
	}

	err = amo.account()
	if err != nil {
		return &amo, err
	}

	return &amo, err
}


func NewByToken(domain, token string) (*Amo, error) {
	var amo Amo
	if strings.Index(domain, ".amocrm.ru") == - 1 {
		domain = fmt.Sprintf("%s.amocrm.ru", domain)
	}
	amo.Domain = domain
	amo.Token = token

	jar, err := cookiejar.New(&cookiejar.Options{PublicSuffixList: publicsuffix.List})
	if err != nil {
		return &amo, err
	}

	amo.Client = &http.Client{
		Jar: jar,
	}

	err = amo.auth()
	if err != nil {
		return &amo, err
	}

	err = amo.account()
	if err != nil {
		return &amo, err
	}

	return &amo, err
}


func NewByToken2(domain, token string) (*Amo, error) {
	var amo Amo
	if strings.Index(domain, ".amocrm.ru") == - 1 {
		domain = fmt.Sprintf("%s.amocrm.ru", domain)
	}
	amo.Domain = domain
	amo.Token = token

	jar, err := cookiejar.New(&cookiejar.Options{PublicSuffixList: publicsuffix.List})
	if err != nil {
		return &amo, err
	}

	amo.Client = &http.Client{
		Jar: jar,
	}

	err = amo.account()
	if err != nil {
		return &amo, err
	}

	return &amo, err
}
