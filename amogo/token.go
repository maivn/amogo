package amogo

import (
	"net/http"
	"fmt"
	"net/url"
	"bytes"
	"io/ioutil"
	"encoding/json"
	"strings"
)

type AuthErr struct {
	Hint   string `json:"hint"`
	Title  string `json:"title"`
	Type   string `json:"type"`
	Status int64  `json:"status"`
	Detail string `json:"detail"`
}

type Params struct {
	ClientId     string `json:"client_id"`
	ClientSecret string `json:"client_secret"`
	GrantType    string `json:"grant_type"`
	Code         string `json:"code"`
	RefreshToken string `json:"refresh_token"`
	RedirectUri  string `json:"redirect_uri"`
}

type Response struct {
	TokenType    string `json:"token_type"`
	ExpiresIn    int64  `json:"expires_in"`
	AccessToken  string `json:"access_token"`
	RefreshToken string `json:"refresh_token"`
}

func (e *AuthErr) Error() string {
	return fmt.Sprintf("%s: %s", e.Hint, e.Detail)
}

func AccessToken(domain string, params Params) (response Response, err error) {
	u := url.URL{}
	u.Scheme = "https"
	u.Path = "/oauth2/access_token"
	if strings.Index(domain, ".amocrm.ru") == -1 {
		domain = fmt.Sprintf("%s.amocrm.ru", domain)
	}
	u.Host = domain

	data, err := json.Marshal(struct {
		ClientId     string `json:"client_id"`
		ClientSecret string `json:"client_secret"`
		GrantType    string `json:"grant_type"`
		Code         string `json:"code"`
		RefreshToken string `json:"refresh_token"`
		RedirectUri  string `json:"redirect_uri"`
	}{
		params.ClientId,
		params.ClientSecret,
		params.GrantType,
		params.Code,
		params.RefreshToken,
		params.RedirectUri,
	})
	if err != nil {
		return
	}

	resp, err := http.Post(u.String(), "application/json", bytes.NewReader(data))
	if err != nil {
		return
	}
	defer resp.Body.Close()
	data, err = ioutil.ReadAll(resp.Body)
	if err != nil {
		return
	}

	var authErr AuthErr
	err = json.Unmarshal(data, &authErr)
	if err != nil {
		return
	}
	if authErr.Status != 0 {
		err = &authErr
		return
	}

	err = json.Unmarshal(data, &response)
	return
}
