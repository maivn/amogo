package main

import (
	"strings"
	"amogo/amogo"
	"fmt"
)

type Array map[string]interface{}

func (a *Array) writeValue(keys []string, value string) {
	n := a
	for i, key := range keys {
		if !n.existKey(key) {
			m := make(Array)
			(*n)[key] = &m
		}
		if i != len(keys)-1 {
			n = (*n)[key].(*Array)
		} else {
			(*n)[key] = value
		}
	}
}

func (a *Array) existKey(key string) bool {
	for k := range *a {
		if k == key {
			return true
		}
	}
	return false
}

func main() {

	client, err := amogo.NewByToken("testaccount1001", "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImQ1MWQxYTcwMDM5OTc4MzE2ZmZjY2NkZjQ4ZmQ1N2UzNjliMTdlY2U3ZWFkNTRjOWM5OGU5YzFjYjc2MGU3OTYwY2FkN2M2ZTAzYmYzZWYyIn0.eyJhdWQiOiJmMWUxNzk0Ni0wYjVhLTQwY2YtOWY1Yi0zNTBiNzQ1Yjg1ZDciLCJqdGkiOiJkNTFkMWE3MDAzOTk3ODMxNmZmY2NjZGY0OGZkNTdlMzY5YjE3ZWNlN2VhZDU0YzljOThlOWMxY2I3NjBlNzk2MGNhZDdjNmUwM2JmM2VmMiIsImlhdCI6MTU5MzYyNzU2MiwibmJmIjoxNTkzNjI3NTYyLCJleHAiOjE1OTM3MTM5NjIsInN1YiI6IjYxNDc2MDciLCJhY2NvdW50X2lkIjoyODk1MTU3OSwic2NvcGVzIjpbInB1c2hfbm90aWZpY2F0aW9ucyIsImNybSIsIm5vdGlmaWNhdGlvbnMiXX0.aie0cuFyxbrDjPu93Z-x4OQtsRm70ELDDDim_7iqdplQS56yBCHSBs4Tf25UE4-N73LRH-2K3qej0OpINZgp3FhAQtcY1qDT8huWAajERoJ9_hZFiY5U5h8HCT1puNJrdOL6HQZBROSD40qzIuV_QQJ7iQ_jKHYA0j29VQV9eCr0zPrHEfooJyjAS_x5SJNgV6rbVtWaepdsdbE1sXzoBXZixYXQCpZfA_iiSHZyfLUu-OGIxLJ-GSuqcPyJeN6WrL711UJk0Lc9msPYzjBU-X7VZ0oMsf-sfx8GDMBTKImnyE2MWX5gBeFTKTZ6ecjBnSftgRD5rNdh8OtvOBTw3A")
	if err != nil {
		panic(err)
	}

	fmt.Println(client)
}

func parseRawKey(rawKey string) (keys []string) {
	for _, k := range strings.Split(rawKey, "[") {
		keys = append(keys, strings.TrimRight(k, "]"))
	}
	return
}
